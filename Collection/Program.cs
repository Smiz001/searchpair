﻿using System;
using System.Collections.Generic;

namespace Collection
{
  class Program
  {
    static void Main(string[] args)
    {
      var arr = ImplementationList();
      SearchPair(arr, 36);
      Console.ReadLine();
    }

    private static void SearchPair(List<int> ar, int value)
    {
      ar.Sort();
      for (int i = 0; i < ar.Count; i ++)
      {
        Console.Write(ar[i] + " ");
      }
      Console.WriteLine();
      int start = 0;
      int end = ar.Count - 1;
      while (start < end)
      {
        int s = ar[start] + ar[end];
        if (s == value)
        {
          Console.WriteLine("Пара чисел: " + ar[start] + " " + ar[end]);
          //Учитываются однаковые пары чисел
          //Если их не учитывать, то необходимо добавить start++;
          end--;
        }
        else
        {
          if (s < value)
            start++;
          else
            end--;
        }
      }
    }

    private static List<int> ImplementationList()
    {
      int size = 20;
      List<int> arr = new List<int>(size);
      Random rand = new Random();
      for (int i = 0; i < arr.Capacity; i++)
      {
        var val = rand.Next(1, 45);
        arr.Add(val);
      }
      return arr;
    }
  }

}
